#include <math.h>
#include <stdio.h>


double normpdf(double x, double mu, double std);

/*
 *
 *
 */
double resolution_thres_void(int n, double* x, void *user_data) {
    double *data = (double *) user_data;

    double result = 0;
    double sigma, thres;
    sigma = data[1];
    thres = data[2];

    int size = (int) data[0];
#ifdef __DEGUB
    printf("Length: %d\n", size);
    printf("Sigma: %e\n", sigma);
    printf("Threshold: %e\n", thres);
    printf("x[0]: %e\n", x[0]);
#endif

    if (x[0] > thres){
        int i;
        for(i=0; i<size; i++){
            result =  result + (data[i+size+3] * normpdf(x[0], data[i+3], sigma));
        }
#ifdef __DEBUG
        printf("testresult: %e\n", (data[i+size+3] * normpdf(x[0], data[i+3], sigma)));
#endif
    }
    //FILE *outfile;
    //outfile = fopen( "resolution.txt", "w");
    //fputs((char *)&size, outfile);
    //fclose( outfile );
#ifdef __DEBUG
    printf("Result: %e\n", result);
#endif
    return result;
}

// normed Gauss distribution
double normpdf(double x, double mu, double std){
    return exp(-((x-mu)*(x-mu)/(std*std*2)))/(sqrt(2*M_PI)*std);
}
