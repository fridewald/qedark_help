"""  handles the output of QEdark

class QEdarkData:
\t helps to handle data from qedark
\t uses libresolution.c for fast integration,
\t in case you have not compiled it, do it.

functions:
\t thres_fermi, thres_erf & thres_simple for different cut-offs
\t resolution, resolution_thres, resolution_thres_erf mimic the mmc resolution
"""

import time
import random
import json
import ctypes

import numpy as np
import scipy.special as ssp
import scipy.stats as sstats
import scipy.integrate as sintegrate
from scipy import LowLevelCallable
RESOLUTION_PATH = '/kalinka/home/lukwata/testQEdark/qedark_help/libresolution.so'

class QEdarkData(object):
    """contains all importent info of the output data of QEdark, for germanium"""
    def __init__(self, imass, **kwargs):
        """
        args:
            imass : int
                number of massfile starting with 1
        kwargs:
            profile_i : [0, 1, 2, 3, 4, 5, 6, 7, 8]
                numer of the profile corresponding the month & dm-formfactor
                0 : December (Subscript[v, Earth]=225 km/s) for Subscript[F, DM](q) = 1.
                1 : December for Subscript[F, DM](q) = Subscript[\[Alpha]m, e]/q.
                2 : December for Subscript[F, DM](q) = (Subscript[\[Alpha]m, e]/q)^2.
                3 : March (Subscript[v, Earth]=240km/s) for Subscript[F, DM](q) = 1.
                4 : March for Subscript[F, DM](q) = Subscript[\[Alpha]m, e]/q.
                5 : March for Subscript[F, DM](q) = (Subscript[\[Alpha]m, e]/q)^2.
                6 : June (Subscript[v, Earth]=255 km/s) for  Subscript[F, DM](q) = 1.
                7 : June for Subscript[F, DM](q) = Subscript[\[Alpha]m, e]/q.
                8 : June for Subscript[F, DM](q) = (Subscript[\[Alpha]m, e]/q)^2.
            debug : bool
                turn debugging on/off, default=False
            mass_file : string
                set json-file which contains the masses
            indir : string
                directory where rate is stored
            seed : int, float, string
                value of seed for random generator
        """
        # constants
        self.c = 2.99792458e10       #[cm/s]
        self.sig_test = 1e-37        # cm^-2
        self.Vcell = 316.4269        # bohr^3
        self.Vcell_eV = self.Vcell * (2.68e-4)**3 # 1/eV^3
        self.Mcell = 2 * 145.28 * 1.66053904e-27  # in kg
        self.rho_x = 0.4e9           # DM density [eV/cm^3]
        # json file in which the masses are stored in eV
        file = kwargs.get("mass_file", "masses.json")
        with open(file, "r") as f:
            #masses in MeV
            masses = json.load(f)
        self.M_x = masses[imass-1]*1.e6 # DM mass [eV]

        self.M_e = 511.0e3      # Electron mass [eV]
        self.mu_x = (self.M_x * self.M_e)/(self.M_x + self.M_e)  # reduced mass [eV]
        self.delta_E = 2.9           # eV
        self.E_gap = 0.67            # eV
        self.alpha = 1/137           # feinstruktur Konstante
        # 10^5 to convert from [km/s]^-1 to [cm/s]^-1, 2.68E-4 to convert from bohr to eV
        self.qe_to_si = 1/(1e5)*2.68e-4
        self.sec_to_year = 3600 * 24 * 365.25
        # unit of plot_data = [bohr/(km/s)]
        # for rate in events/kg/year
        self.prefac_test = \
            self.c**2 / self.Mcell * self.rho_x/self.M_x \
            * self.sig_test/self.mu_x**2 * 2 * np.pi**2 / self.Vcell_eV \
            * self.sec_to_year * self.qe_to_si
        self.prefac = \
            self.c**2 / self.Mcell * self.rho_x / self.M_x / self.mu_x**2 \
            * 2 * np.pi**2 / self.Vcell_eV * self.sec_to_year * self.qe_to_si
        self.prefac_tien = \
            self.sig_test*self.c**2 / self.Mcell * self.rho_x / self.M_x \
            / self.mu_x**2 * 1/(2**5 * np.pi**4) * self.Vcell \
            * self.sec_to_year * self.qe_to_si/(2.68e-4)**3

        # load input data
        filename = "C.{0:02d}.dat".format(imass)
        indir = kwargs.get("indir", "./")
        data = np.loadtxt(indir+filename)
        # col 1 FDM
        # col 2 time of year
        # col 3 sum of all col 4-504
        # col 4-504 rate, bin-width = 0.1eV
        self.nbins = 500
        # [eV]
        self.energy_steps = 0.1
        # number of bins for bin_edges
        self.edge_bins = int((self.nbins * self.energy_steps - self.E_gap) / self.delta_E + 1)
        # row 3 march v_rel = 15 km/s
        self.profile_i = kwargs.get("profile_i", 5)

        # generate data
        self.total_energy = self.prefac_test * data[self.profile_i, 2]
        # data for rate
        self.plot_data = data[self.profile_i, 3:503]
        self.normed_plot_data = self.plot_data / self.total_energy
        self.energy = np.linspace(0, self.nbins*self.energy_steps, self.nbins+1)
        # bin_edges in eV
        self.bin_edges = np.linspace(self.E_gap,
                                     self.edge_bins*self.delta_E+self.E_gap,
                                     self.edge_bins+1)
        # rate with prefac
        (self.rate_sig, self.rate) = ([] for _ in range(2))
        for i in self.bin_edges[:-1]/self.energy_steps-1:
            self.rate_sig.append(self.prefac * np.sum(self.plot_data[int((i)):int((i+self.delta_E/self.energy_steps))]))
        self.rate = self.sig_test * np.array(self.rate_sig)
        if kwargs.get("debug", False):
            print("Total rate: " + str(self.total_energy))
            print("Total rate own sum: " + str(self.get_tot_rate()))
            print("FFdm: " + str(data[self.profile_i, 0]))
            print("month: " + str(data[self.profile_i, 1]))

        self.randonios = random.Random(kwargs.get('seed', time.time))

    def get_tot_rate(self, threshold=1):
        """threshold in elemental charge"""
        return sum(self.rate[threshold-1:])

    def get_tot_rate_for_sig(self, threshold=1):
        """total rate for sigma calculation"""
        return sum(self.rate_sig[threshold-1:])

    def get_tot_rate_tien(self, threshold=1):
        """total rate for data tien gave me"""
        return self.prefac_tien/self.prefac/self.sig_test*sum(self.rate_sig[threshold-1:])

    def get_tot_rate_mmc(self, volt=100, **kwargs):
        """
        total rate after after neganov-luke-effec and  mmc-effect are applied

        args:
            volt : float, optional, default=100
                voltage applied [V]
        kwargs:
            sigma : float, optional, default=1
                standard deviation
            same as self.resolution
        return:
            tuple (value, error)
        """
        return sintegrate.quad(lambda x: resolution(x, self.rate_sig,
                                                    self.get_neganov_list(volt),
                                                    **kwargs),
                               0, volt*self.get_ax2_tick_labels()[-1])

    def get_tot_rate_mmc_thres(self, volt=100, **kwargs):
        """
        total rate after after neganov-luke-effec and  mmc-effect are applied
        use c-code which  is much faster

        args:
            volt : float, optional, default=100
                voltage applied [V]
        kwargs:
            sigma : float, optional, default=1
                standard deviation
            thres: float, optionl, default=100
                threshold until distribution is zero
        return:
            tuple (value, error)
        """
        sig = kwargs.get('sigma', 1)
        thres = kwargs.get('thres', 100)

        # import c code
        lib = ctypes.CDLL(RESOLUTION_PATH)

        # set input and output types of c code
        lib.resolution_thres_void.restype = ctypes.c_double
        lib.resolution_thres_void.argtypes = (ctypes.c_int,
                                              ctypes.POINTER(ctypes.c_double),
                                              ctypes.c_void_p)

        # set data for c function
        length = float(len(self.rate_sig))
        lDoubleArrayType = ctypes.c_double * int(length * 2 + 4)

        c_list = [length, sig, thres]
        c_list.extend(self.get_neganov_list(volt))
        c_list.extend(self.rate_sig)
        c = lDoubleArrayType(*c_list)
        # cast data, cause LowLevelCallable accepts only c_void_p
        # needs to be recasted in c code
        user_data = ctypes.cast(ctypes.pointer(c), ctypes.c_void_p)

        # define LowLevelCallable and set data
        func = LowLevelCallable(lib.resolution_thres_void, user_data)

        # result
        res = sintegrate.quad(func, 0, volt*self.get_ax2_tick_labels()[-1])
        return res


    def get_tot_rate_mmc_thres_simple(self, volt=100, **kwargs):
        """
        total rate after after neganov-luke-effec and  mmc-effect are applied
        simple threshold function

        args:
            volt : float, optional, default=100
                voltage applied [V]
            sigma : float, optional, default=1
                standard deviation
            thres: float, optionl, default=100
                threshold until distribution is zero
        return:
            tuple (value, error)
        """
        return sintegrate.quad(
            lambda x: resolution_thres(x, self.rate_sig,
                                       self.get_neganov_list(volt),
                                       **kwargs),
            0, volt*self.get_ax2_tick_labels()[-1])

    def get_tot_rate_mmc_thres_erf(self, volt=100, **kwargs):
        """
        total rate after after neganov-luke-effec and  mmc-effect are applied
        erf-functin as threshold function

        args:
            volt : float, optional, default=100
                voltage applied [V]
            sigma : float, optional, default=1
                standard deviation
            thres: float, optionl, default=100
                threshold until distribution is zero
        return:
            tuple (value, error)
        """
        return sintegrate.quad(
            lambda x: resolution_thres_erf(x, self.rate_sig,
                                       self.get_neganov_list(volt),
                                       **kwargs),
            0, volt*self.get_ax2_tick_labels()[-1])

    def get_ax2_tick_location(self):
        """energy that equals the number of e-"""
        return self.bin_edges[1:] - 0.5*self.delta_E

    def get_ax2_tick_labels(self):
        """equals number of electrons"""
        return np.arange(1, (self.nbins*self.energy_steps - self.E_gap)/self.delta_E + 1)

    def get_random_event(self):
        """get random data following the eventrate"""
        # highes value of normed_plot_data
        top = max(self.normed_plot_data)
        while True:
            y_rand = self.randonios.uniform(0, top)
            x_rand = self.randonios.randrange(0, self.nbins)
            if y_rand <= self.normed_plot_data[x_rand]:
                break
        return x_rand

    def get_neganov_list(self, volt=100):
        """
        neganov-luke boosted signal

        args:
            volt: float, default=100
                voltage applied [V]
        return: list
        """
        return list(map(lambda x: self.neganov_boost(x, volt), self.get_ax2_tick_labels()))

    def neganov_boost(self, num_q, volt):
        """
        boost the number of electrons to energy with neganov-luke-effect

        args:
            num_q: int
                number of electrons
            volt: float
                voltage applied [V]
        return: float
        """
        return (num_q-1)*(self.delta_E-self.E_gap) + num_q*volt


def thres_fermi(x, mu=0, sig=1):
    """fermi-function, modified for my needs"""
    return 1/(np.exp(-(x-mu)*np.sqrt(2)/sig)+1)

def thres_erf(x, mu=0, sig=1):
    """
    CDF for normal distribution

    args:
        x: ndarray, float
            x-points where to calculate
        mu: float
            mean
        sig: float
            std
    """
    return 1/2*(ssp.erf((x-mu)/np.sqrt(2)/sig)+1)

def thres_simple(x, threshold):
    """
    simple threshold function

    args:
        x: ndarry, float

        thres: float
    return: x-like
    """
    return np.where(x > threshold, 1, 0)

def resolution(x, fak, mu, **kwargs):
    """
    resolution of MMC
    fak, mu should be an array or list
    args:
        x : np.array, number
            x-value/s
        fak : array_like
            list of faktors multiplied with normal-distribution
        mu : array_like
            list of mean-values for normal-distribution
        sigma : float, optional, default=1
            standard deviation
    """
    sig = kwargs.get('sigma', 1)
    return np.sum([fak_i * sstats.norm.pdf(x, mu_i, sig) for fak_i, mu_i in zip(fak, mu)],
                  axis=0)

def resolution_thres(x, fak, mu, **kwargs):
    """
    resolution of MMC and threshold
    fak, mu should be an array or list
    args:
        x : np.array, number
            x-value/s
        fak : array_like
            list of faktors multiplied with normal-distribution
        mu : array_like
            list of mean-values for normal-distribution
        sigma : float, optional, default=1
            standard deviation
        thres: float, optionl, default=100
            threshold until distribution is zero
    """
    sig = kwargs.get('sigma', 1)
    thres = kwargs.get('thres', 100)
    return np.sum([fak_i * sstats.norm.pdf(x, mu_i, sig)*thres_simple(x, thres) for fak_i, mu_i in zip(fak, mu)],
                  axis=0)

def resolution_thres_erf(x, fak, mu, **kwargs):
    """
    resolution of MMC and threshold
    fak, mu should be an array or list
    args:
        x : np.array, number
            x-value/s
        fak : array_like
            list of faktors multiplied with normal-distribution
        mu : array_like
            list of mean-values for normal-distribution
        sigma : float, optional, default=1
            standard deviation
        thres: float, optionl, default=100
            threshold until distribution is zero
    """
    sig = kwargs.get('sigma', 1)
    thres = kwargs.get('thres', 100)
    return np.sum([fak_i * sstats.norm.pdf(x, mu_i, sig)*thres_erf(x, thres, 0.1) for fak_i, mu_i in zip(fak, mu)],
                  axis=0)
