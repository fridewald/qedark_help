CC=gcc
CFLAGS=-shared -Wl
LIBFLAGS=-fPIC
OBJ=resolution_thres.o


%.o : %.c
	$(CC) -c -o $@ $< $(LIBFLAGS)

libresolution.so : $(OBJ)
	$(CC) $(CFLAGS),-soname,$@ -o $@ $^

m.PHONY : clean

clean :
	rm -vf libresolution.so resolution_thres.o
